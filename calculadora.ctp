<?php
use Cake\Utility\Inflector;
$breadcrums = $this->cell('Breadcrum', [$product->category->id, $product->title]);
$related = $this->cell('Related', [$product->category->id]); // Productos relacionados
$this->assign('title', 'Producto: '.$product->title);
$this->layout = 'frontend';
?>

<?php
$url = "http://$_SERVER[HTTP_HOST]";
$full_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$parse_url = parse_url($full_url);
$filename_url = substr(strrchr($parse_url['path'], "/"),1);
?>

<?php
	$this->append('meta', $this->Html->meta(['property' => 'og:url', 'content' => $full_url]));
	$this->append('meta', $this->Html->meta(['property' => 'og:type', 'content' => 'website']));
	$this->append('meta', $this->Html->meta(['property' => 'og:title', 'content' => $product->title]));
	$this->append('meta', $this->Html->meta(['property' => 'og:description', 'content' => $product->description]));
	$this->append('meta', $this->Html->meta(['property' => 'og:image', 'content' => $url.'/files/products/photo/'.$product->photo_dir.'/thumb_'.$product->photo]));
?>


<section class="calculadora">
		<div class="header text-center">
		<h1>- Calculadora de materiales -</h1>
	</div>
	<div class="container">
		<div class="row calculadora">
			<div class="col-md-8 col-md-offset-2">
				<form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Linea:</label>
                <div class="col-sm-10">
                    <select id="selectLinea" class="form-control">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Producto:</label>
                <div class="col-sm-10">
                    <select id="selectProducto" class="form-control">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Herramiena de aplicacion:</label>
                <div class="bs-example col-sm-6" id="aplicacion">
                    <span class="icon_aplicacion  pistola select">Pistola</span>
                    <span class="icon_aplicacion brocha">Brocha</span>
                    <span class="icon_aplicacion rodillo">Rodillo</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tipo de superficie:</label>
                <div class="bs-example col-sm-6">
                    <span class="icon_superficie liso select">Lisa </span>
                    <span class="icon_superficie rugoso">Rugosa</span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Numero de manos:</label>
                <div class="col-sm-2">
                    <input id="manos" class="form-control" type="number" min="1" max="10" placeholder="Numero de manos" value="1">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Metros cuadrados:</label>
                <div class="col-sm-2">
                    <input id="metros" class="form-control" type="number" min="1" max="10" placeholder="Metros cuadrados" value="1">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-1 col-sm-offset-2">
                    <button id="calcular" type="button" class="btn btn-primary  active">Calcular</button>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Litros necesarios:</label>
                <div class="col-sm-2">
                    <input id="resultado" type="text" class="form-control" placeholder="LItros necesarios" disabled>
                </div>
            </div>
        </form>
			</div>
    </div>
	</div>
</section>

<!-- Variables -->
<?= $product->id ?><br>
<?= $product->lisa ?><br>
<?= $product->rugosa ?><br>
<?= $product->category_id ?><br>


<pre>
<?= var_dump($product); ?>
</pre>
