<?= $this->prepend('script', $this->Html->script([
		'/bower_components/jquery/dist/jquery.min',
		'/bower_components/bootstrap/dist/js/bootstrap.min',
		'/rs-plugin/js/jquery.themepunch.revolution.min',
		'/rs-plugin/js/jquery.themepunch.plugins.min',
		'/bower_components/wow/dist/wow.min',
		'sPanels.min',
		'main'
		]));
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Pinturas Valmex | <?= $this->fetch('title') ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?= $this->fetch('meta') ?>
	
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	
	<?= $this->Html->css([
			'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
			'/bower_components/bootstrap/dist/css/bootstrap.min',
			'/rs-plugin/css/settings',
			'main'
			]);
	?>
			
	<script src="/js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
	
<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

	<?= $this->element('menu') ?>
	
	<?= $this->fetch('content') ?>
	
	<?= $this->element('footer') ?>
	
	<?= $this->fetch('script'); ?>
	

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
		e.src='//www.google-analytics.com/analytics.js';
		r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
		ga('create','UA-XXXXX-X','auto');ga('send','pageview');
	</script>
	
	<script>
    $(function(){
      // bind change event to select
      $('.categoria').on('change', function () {
          var id = $(this).val(); // get selected value
          if (id) { // require a URL
              window.location = id; // redirect
          }
          return false;
      });
    });
    
	</script>
	
	<script>
function calculadora() {
    'use strict'

    var id_category = "",
        apiResponse = {},
        aplicacion = 1,
        superficie = 0,
        tipoSuperficie = 0,
				php_id = 0,
				php_category_id = 0;

    $(document).ready(function() {
			php_id = "<?php echo $product -> id?>";
			php_category_id = "<?php echo $product -> category_id?>";
      xhrCall();
    });

    function xhrCall() {
        var url = 'http://valmex.bolder.com.mx/categories/categoriesApi.json';
        var method = 'GET';
        var xhr = createCORSRequest(method, url);
        xhr.onload = function() {
            apiResponse = jQuery.parseJSON(xhr.responseText);
            getLine(apiResponse)
            xhr.onerror = function() {
                console.log("No se pudo recibir respuesta del servidor =(")
            };
        }

        xhr.send();

        function createCORSRequest(method, url) {
            var xhr = new XMLHttpRequest();
            if ("withCredentials" in xhr) {
                // Most browsers.
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest != "undefined") {
                // IE8 & IE9
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported.
                xhr = null;
            }
            return xhr;
        };
    }

    function getLine(line) {
        var options = $("#selectLinea");
        options.append('<option value=""  disabled selected>Escoge linea</option>')
        for (var i = 0; i < line.categories.length; i++) {
            var obj = line.categories[i];
            if (obj.products != 0) {
                options.append($("<option />").val(obj.name).text(obj.name).attr("id", obj.id));
            }
        }
				if (php_category_id != 0) {
					$("#selectLinea option[id="+ php_category_id +"]").attr("selected", "selected");
					id_category = php_category_id;
					getProduct(apiResponse);
					$("#selectProducto option[id="+ php_id +"]").attr("selected", "selected");
					// $(".icon_superficie").removeClass("select");
	        // $(".icon_aplicacion").removeClass("select");
				}

    };

    function getProduct(product) {
        var options = $("#selectProducto");
        options.find('option').remove();
        for (var i = 0; i < product.categories.length; i++) {
            var obj = product.categories[i].products;
            if (obj.length > 0) {
                if (obj[0].category_id == id_category) {
                    jQuery.each(obj, function(index, val) {
                        //options.append($("<option />").val(val.title).text(val.title).attr("id", val.id));
                        options.append($("<option />").val(val.rugosa).text(val.title).attr("id", val.id).attr("data-lisa", val.lisa));
                    });
                };
            }
        }
    };

    $("#selectLinea")
        .change(function() {
            $("#selectLinea option:selected").each(function() {
                id_category = "";
                id_category = Number($(this).attr("id"));
                getProduct(apiResponse);
            });
        })
        .trigger("change");

    $(".icon_aplicacion").click(function() {
        aplicacion = $(".icon_aplicacion").index(this);
        switch (aplicacion) {
            case 1:
                aplicacion = 10.8;
                break;
            case 2:
                aplicacion = 17.7;
                break;
        }
        $(this).parent().find('.select').toggleClass('select');
        $(this).toggleClass('select');
    });

    $(".icon_superficie").click(function() {
        superficie = $(".icon_superficie").index(this);
        superficie = (superficie === 0) ? $("#selectProducto option:selected").data("lisa") : $("#selectProducto").val();
        $(this).parent().find('.select').toggleClass('select');
        $(this).toggleClass('select');
    });

    $("#calcular").on('click', function() {
        var producto = superficie;
				superficie = (superficie === 0) ? $("#selectProducto option:selected").data("lisa") : $("#selectProducto").val();
        var manos = Number($("#manos").val());
        var metros = Number($("#metros").val());
        var porcent1 = 0,
            porcent2 = 0;

        producto = metros * producto;

        if (aplicacion === 10.8) {
            porcent1 = (aplicacion / 100) * producto;
            console.log(porcent1)
        } else if (aplicacion === 17.7) {
            porcent1 = (aplicacion / 100) * producto;
            console.log(porcent1)
        }

        if (superficie === 11.2) {
            porcent2 = (superficie / 100) * producto
            console.log(porcent2);
        }

        var total = (producto + porcent1 + porcent2) * manos;
        $("#resultado").val(Math.round(total * 10) / 10);

        $(".icon_superficie").removeClass("select");
        $(".icon_aplicacion").removeClass("select");

    })
};
calculadora ();
</script>


</body>

</html>