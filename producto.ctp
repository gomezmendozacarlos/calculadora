<?php
use Cake\Utility\Inflector;
$breadcrums = $this->cell('Breadcrum', [$product->category->id, $product->title]);
$related = $this->cell('Related', [$product->category->id]); // Productos relacionados
$this->assign('title', 'Producto: '.$product->title);
$this->layout = 'frontend';
?>

<?php
$url = "http://$_SERVER[HTTP_HOST]";
$full_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$parse_url = parse_url($full_url);
$filename_url = substr(strrchr($parse_url['path'], "/"),1);
?>

<?php 
	$this->append('meta', $this->Html->meta(['property' => 'og:url', 'content' => $full_url]));
	$this->append('meta', $this->Html->meta(['property' => 'og:type', 'content' => 'website']));
	$this->append('meta', $this->Html->meta(['property' => 'og:title', 'content' => $product->title]));
	$this->append('meta', $this->Html->meta(['property' => 'og:description', 'content' => $product->description]));
	$this->append('meta', $this->Html->meta(['property' => 'og:image', 'content' => $url.'/files/products/photo/'.$product->photo_dir.'/thumb_'.$product->photo])); 
?> 


<section class="productos">
	<?= $breadcrums ?>
	<img src="/img/brushes/red-up.png" class="img-responsive no-print">
	<div class="container">
		
		
		<div class="row">
			<div class="col-sm-4 col-md-4 text-center">
				<?= $this->Html->link('<img src="../img/back.png" width="40"> Regresar a productos',
					['controller' => 'Categories' , 'action' => 'categorias',
					'id' => $product->category->id, 'slug' => Inflector::slug(strtolower($product->category->name))],
					['class'=>'back-btn','escape' => false, 'title'=>'Productos']);
				?>
				<?= $this->Html->image('../files/products/photo/' . $product->photo_dir . '/' . $product->photo, ['class' => 'img-responsive','alt'=>$product->title]); ?>
				
				<?= $this->Html->link('<img src="../img/pdf.png" width="60"> Hoja técnica',
					['action' => 'ficha', $product->id],
					['class'=>'back-btn no-print','escape' => false, 'title'=>'Descarga Hoja técnica']);
				?>

				<!-- PRODUCTOS RELACIONADOS -->
				<div class="hidden-xs">
					<div class="row related-products">
						<div class="col-md-12 text-center">
							<br><h3>Productos relacionados</h3><br>
						</div>
						<?php foreach ($product->relateds as $relateds): ?>
						<div class="col-xs-6 col-sm-6 col-md-6 text-center product">
							<?= $this->Html->link($this->Html->image('../files/products/photo/' . $relateds->photo_dir . '/' . $relateds->photo, ['width' => '80']),
				['controller' => 'products' , 'action' => 'producto', 'id' => $relateds->id, 'slug' => Inflector::slug($relateds->title)],['escape'=>false]);?>
							<p><?= $this->Html->link($this->Text->truncate($relateds->title, 25, ['ellipsis' => '...', 'exact' => true]),
								['controller' => 'products' , 'action' => 'producto', 'id' => $relateds->id, 'slug' => Inflector::slug($relateds->title)]);?></p>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-md-8 caracteristicas">
				<?php if(!empty($product->category->photo)):?>
					<a href="/categorias/<?=$product->category->id?>/<?=Inflector::slug(strtolower($product->category->name))?>"><?= $product->has('category') ? $this->Html->image('../files/categories/photo/'.$product->category->photo_dir.'/'.$product->category->photo, ['width'=>'350','class' => '']) : '' ?></a>
					<br><br>
				<?php endif;?>
				
				<h1 class="quote"><?= $product->frase ?></h1>
				<h1><?= $product->title ?></h1>
				<h2><?= $product->nombre_generico ?></h2>
				<h2 style="color: grey"><?= $product->uso ?></h2>
				<h3>Terminado:</h3>
				<p><?= $product->terminado ?></p>
				<h3>Descripción:</h3>
				<p><?= nl2br($product->description) ?></p>
				<h3>Usos:</h3>
				<p><?= nl2br($product->usos) ?></p>
				<h3>Tiempo de secado:</h3>
				<p><?= $product->tiempo?></p>
				<p class="extras"><?= nl2br($product->extras) ?></p>
				
				<?php if (!empty($product->sizes)): ?>
					<h3>Presentaciones:</h3><br>
					<?php foreach ($product->sizes as $sizes): ?>
				    <?= $this->Html->image('../files/sizes/photo/' . $sizes->photo_dir . '/' . $sizes->photo, ['class'=>'','width'=>'100','data-toggle'=>'tooltip','data-placement'=>'bottom','data-original-title'=>$sizes->name]);
				    ?>
					<?php endforeach; ?>
					<div class="clearfix"></div><br>
				<?php endif; ?>
				
				
				<?php if (!empty($product->colors)): ?>
					<h3>Muestrario de color *</h3><br>
					<?php foreach ($product->colors as $colors): ?>
						<div class="color" style="background: <?= $colors->color ?>"
						data-html="true" 
						data-toggle="tooltip" 
						data-placement="bottom" 
						data-original-title="<?= $colors->title ?> <br> <?= $colors->code?>">
						</div>
					<?php endforeach; ?>
					<div class="clearfix"></div>
					<p class="info"><i>* Colores aproximados, pueden tener una pequeña diferencia con los reales.</i></p><br>
				<?php endif; ?>
				
				<?php if (!empty($product->characteristics)): ?>
					<h3>Características a resaltar:</h3><br>
					<?php foreach ($product->characteristics as $characteristics): ?>
						<div class="botones">
				    <?=$this->Html->image('../files/characteristics/photo/' . $characteristics->photo_dir . '/' . $characteristics->photo, ['class'=>'','width'=>'100','data-toggle'=>'tooltip','data-placement'=>'bottom','data-original-title'=>$characteristics->name]);
				    ?>
				    </div>
					<?php endforeach; ?>
					<div class="clearfix"></div><br>
				<?php endif; ?>
					<div class="botones no-print">
						<?php if ($product->simulador == 1): ?>
							<?= $this->Html->link($this->Html->image('../img/simulador.png', ['class'=>'zoom', 'width' => '80' , 'alt'=>'Simulador de colores']),
								['controller' => 'products' , 'action' => 'colores', 'id' => $product->id, 'slug' => Inflector::slug($product->title)],
								['escape' => false, 'title'=>'Simular los colores de '.$product->title]);
							?>
							<h2>Simulador<br> de colores</h2>
						<?php endif; ?>
					</div>
					<div class="botones no-print">
							<?= $this->Html->link($this->Html->image('../img/calculadora.png', ['class'=>'zoom', 'width' => '80' , 'alt'=>'Calculadora de materiales']),
								['controller' => 'products' , 'action' => 'calculadora', 'id' => $product->id, 'slug' => Inflector::slug($product->title)],
								['escape' => false]);
							?>
						<h2>Calculadora <br>de materiales</h2>
					</div>
					<div class="botones no-print">
						<a href="#" onClick="window.print()">
						<?= $this->Html->image('../img/print.png', ['class'=>'zoom', 'width' => '80' , 'alt'=>'imprimir']); ?>
						</a>
						<h2>Imprimir</h2>
					</div>
					<div class="clearfix"></div>
				<!-- PRODUCTOS RELACIONADOS -->
				<div class="hidden-sm hidden-md hidden-lg">
					<div class="row related-products">
						<div class="col-md-12 text-center">
							<br><h3>Productos relacionados</h3><br>
						</div>
						<?php foreach ($product->relateds as $relateds): ?>
						<div class="col-xs-6 col-sm-6 col-md-6 text-center product">
							<?= $this->Html->link($this->Html->image('../files/products/photo/' . $relateds->photo_dir . '/' . $relateds->photo, ['width' => '80']),
				['controller' => 'products' , 'action' => 'producto', 'id' => $relateds->id, 'slug' => Inflector::slug($relateds->title)],['escape'=>false]);?>
							<p><?= $this->Html->link($this->Text->truncate($relateds->title, 25, ['ellipsis' => '...', 'exact' => true]),
								['controller' => 'products' , 'action' => 'producto', 'id' => $relateds->id, 'slug' => Inflector::slug($relateds->title)]);?></p>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<img src="/img/brushes/grey-up.png" class="img-responsive no-print">

<img src="/img/brushes/grey-blue.png" class="img-responsive no-print">

<section class="contacto no-print">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1 class="wow fadeInDown" data-wow-duration="2s">- CONTACTO -</h1>
				<p>Para nosotros es muy importante tu opinión, si tienes alguna pregunta o comentario no<br>
						dudes en ponerte en contacto con nosotros y con mucho gusto te atenderemos.</p>
				<form class="effect2 text-left" id="forma_contacto">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
						    <input class="form-control" type="text" placeholder="NOMBRE *" name="nombre" value="" />
						  </div>
						  <div class="form-group">
						    <input type="email" class="form-control" placeholder="E-MAIL *" name="email" value="">
						  </div>
						  <div class="form-group">
						    <input class="form-control" type="text" placeholder="ASUNTO" name="tel" value="" />
						  </div>
					  </div>
					  <div class="col-md-6">
						  <div class="form-group">
						    <textarea rows="4" class="form-control" placeholder="COMENTARIOS *" name="comentarios"></textarea>
						  </div>
						  <button type="submit" class="btn btn-default">Envia tu mensaje</button>
						</div>
				  </div>
				</form>
			</div>
			
		</div>
	</div>
</section>
