(function() {

    var id_category = '',
        apiResponse = {},
        aplicacion = 1,
        superficie = 0;

    function xhrCall() {
        var url = 'http://valmex.bolder.com.mx/categories/categoriesApi.json',
            method = 'GET',
            xhr = createCORSRequest(method, url);
        xhr.onload = function() {
            apiResponse = jQuery.parseJSON(xhr.responseText);
            getLine(apiResponse);
            xhr.onerror = function() {
                console.log("No se pudo recibir respuesta del servidor");
            };
        }
        xhr.send();

        function createCORSRequest(method, url) {
            var xhr = new XMLHttpRequest();
            if ("withCredentials" in xhr) {
                // Most browsers.
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest != "undefined") {
                // IE8 & IE9
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported.
                xhr = null;
            }
            return xhr;
        };
    }

    $(document).ready(function() {
        xhrCall();
    });

    function getLine(line) {
        var options = $("#selectLinea");
        options.append('<option value=""  disabled selected>Escoge linea</option>')
        for (var i = 0; i < line.categories.length; i++) {
            var obj = line.categories[i];
            if (obj.products != 0) {
                options.append($("<option />").val(obj.name).text(obj.name).attr("id", obj.id));
            }
        }

    };

    function getProduct(product) {
        var options = $("#selectProducto");
        options.find('option').remove();
        for (var i = 0; i < product.categories.length; i++) {
            var obj = product.categories[i].products;
            if (obj.length > 0) {
                if (obj[0].category_id == id_category) {
                    jQuery.each(obj, function(index, val) {
                        //options.append($("<option />").val(val.title).text(val.title).attr("id", val.id));
                        options.append($("<option />").val(val.rugosa).text(val.title).attr("id", val.id).attr("data-lisa", val.lisa));
                    });
                };
            }
        }
    };

    $("#selectLinea")
        .change(function() {
            $("#selectLinea option:selected").each(function() {
                id_category = "";
                id_category = Number($(this).attr("id"));
                getProduct(apiResponse);
            });
        })
        .trigger("change");

    $(".icon_aplicacion").click(function() {
        aplicacion = $(".icon_aplicacion").index(this);
        switch (aplicacion) {
            case 1:
                aplicacion = 10.8;
                break;
            case 2:
                aplicacion = 17.7;
                break;
        }
        $(this).parent().find('.select').toggleClass('select');
        $(this).toggleClass('select');
    });

    $(".icon_superficie").click(function() {
        superficie = $(".icon_superficie").index(this);
        console.log("antes", superficie);
        superficie = (superficie === 0) ? $("#selectProducto option:selected").data("lisa") : $("#selectProducto").val();
        console.log("despues", superficie);
        $(this).parent().find('.select').toggleClass('select');
        $(this).toggleClass('select');
    });

    $("#calcular").on('click', function() {
        var producto = (superficie === 0) ? $("#selectProducto option:selected").data("lisa") : $("#selectProducto").val();
        var manos = Number($("#manos").val());
        var metros = Number($("#metros").val());
        var porcent1 = 0,
            porcent2 = 0;

        producto = metros * producto;

        if (aplicacion === 10.8 && aplicacion === 0) {
            porcent1 = (aplicacion / 100) * producto;
            console.log(porcent1)
        } else if (aplicacion === 17.7) {
            porcent1 = (aplicacion / 100) * producto;
            console.log(porcent1)
        }

        if (superficie === 11.2) {
            porcent2 = (superficie / 100) * producto
            console.log(porcent2);
        }

        console.log(manos);
        var total = (producto + porcent1 + porcent2) * manos;
        $("#resultado").val(Math.round(total * 10) / 10);

        $(".icon_superficie").removeClass("select");
        $(".icon_aplicacion").removeClass("select");

    })
})()
