<?php $this->assign('title', 'Producto: '.$product->title); ?>

<div class="actions columns col-lg-2 col-md-3">
    <h3><?= __('Acciones') ?></h3>
    <ul class="nav nav-stacked nav-pills">
        <li><?= $this->Html->link(__('<i class="fa fa-pencil"></i> Editar producto'), ['action' => 'edit' , $product->id],['escape' => false]) ?></li>
        <li><?= $this->Html->link(__('<i class="fa fa-list-ul"></i> Lista de productos'), ['action' => 'index'],['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-trash"></i> Eliminar',"#",
								        ["class"=>"btn-danger delete-btn",
								        'data-toggle'=>'tooltip','data-placement'=>'right','data-original-title'=>'Eliminar',
								        "escape"=>false,
								        "data-action"=> Cake\Routing\Router::url(array('action'=>'delete',$product->id)), 
								        "data-name"=>$product->title, 
								        //"data-controller"=>$this->request->param('controller')
										]);?>
				</li>
        <li class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-dot-circle-o"></i> Categorías <span class="caret"></span></a>
			    <ul class="dropdown-menu">
			      <li><?= $this->Html->link(__('<i class="fa fa-list-ul"></i> Lista de Categorías'), ['controller' => 'Categories', 'action' => 'index'],['escape' => false]) ?> </li>
						<li><?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Nueva Categoría'), ['controller' => 'Categories', 'action' => 'add'],['escape' => false]) ?> </li>
			    </ul>
			  </li>
			  <li class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-dot-circle-o"></i> Colores <span class="caret"></span></a>
			    <ul class="dropdown-menu">
			      <li><?= $this->Html->link(__('<i class="fa fa-list-ul"></i> Lista de Colores'), ['controller' => 'Colors', 'action' => 'index'],['escape' => false]) ?> </li>
						<li><?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Nuevo Color'), ['controller' => 'Colors', 'action' => 'add'],['escape' => false]) ?> </li>
			    </ul>
			  </li>
			  <li class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-dot-circle-o"></i> Características <span class="caret"></span></a>
			    <ul class="dropdown-menu">
			      <li><?= $this->Html->link(__('<i class="fa fa-list-ul"></i> Lista de Características'), ['controller' => 'Characteristics', 'action' => 'index'],['escape' => false]) ?> </li>
						<li><?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Nueva Característica'), ['controller' => 'Characteristics', 'action' => 'add'],['escape' => false]) ?> </li>
			    </ul>
			  </li>
			  <li class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-dot-circle-o"></i> Tamaños <span class="caret"></span></a>
			    <ul class="dropdown-menu">
			      <li><?= $this->Html->link(__('<i class="fa fa-list-ul"></i> Lista de Tamaños'), ['controller' => 'Sizes', 'action' => 'index'],['escape' => false]) ?> </li>
						<li><?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Nuevo Tamaño'), ['controller' => 'Sizes', 'action' => 'add'],['escape' => false]) ?> </li>
			    </ul>
			  </li>
    </ul>
</div>
<div class="products view col-lg-10 col-md-9 columns">
    <h2><?= h($product->title) ?></h2>
    <div class="row">
        <div class="col-lg-8 columns strings">
            <div class="panel panel-default">
	            	<div class="panel-heading">Información</div>
                <div class="panel-body">
<!--
	                	<h4 class="subheader"><?= __('Id') ?></h4>
                    <p><?= $this->Number->format($product->id) ?></p>
-->
                    <h4 class="subheader"><?= __('Nombre') ?></h4>
                    <p><?= h($product->title) ?></p>
                    <h4 class="subheader"><?= __('En la categoría') ?></h4>
                    <p><?= $product->has('category') ? $this->Html->link($product->category->name, ['controller' => 'Categories', 'action' => 'view', $product->category->id]) : '' ?></p>
                    <h4 class="subheader"><?= __('Descripción') ?></h4>
                    <p><?= h($product->description) ?></p>
                    <h4 class="subheader"><?= __('Creado') ?></h4>
                    <p><?= h($product->created) ?></p>
                    <h4 class="subheader"><?= __('Modificado') ?></h4>
                    <p><?= h($product->modified) ?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 columns numbers end">
            <div class="panel panel-default">
	            	<div class="panel-heading">Imagen</div>
                <div class="panel-body text-center">
                    <?= $this->Html->image('../files/products/photo/' . $product->photo_dir . '/' . $product->photo, ['class' => 'img-responsive']); ?>
                    <h4 class="subheader"><?= __('Nombre de imagen') ?></h4>
                    <p><?= h($product->photo) ?></p>
<!--
                    <h4 class="subheader"><?= __('Directorio imagen') ?></h4>
                    <p><?= h($product->photo_dir) ?></p>
-->
                    <?= $this->Html->link(__('<i class="fa fa-download"></i> Descargar'),'../files/products/photo/' . $product->photo_dir . '/' . $product->photo, 
	                    ['class'=>'btn btn-default', 'download'=>$product->photo,'escape' => false]) ?>
                </div>
            </div>
        </div>
    </div>
		<!--RELACIONES -->
		<div class="related row">
		    <div class="column col-lg-12">
			    <div class="panel panel-default">
		    		<div class="panel-heading">Colores relacionados</div>
						<div class="panel-body">
				    <?php if (!empty($product->colors)): ?>
				    <div class="table-responsive">
				        <table class="table">
				            <tr>
				                <th><?= __('Nombre') ?></th>
				                <th><?= __('Codigo') ?></th>
				                <th><?= __('Color') ?></th>
				                <th><?= __('Creado') ?></th>
				                <th><?= __('Modificado') ?></th>
				                <th class="actions"><?= __('Acciones') ?></th>
				            </tr>
				            <?php foreach ($product->colors as $colors): ?>
				            <tr>
				                <td><?= h($colors->title) ?></td>
				                <td><?= h($colors->code) ?></td>
				                <td style="background: <?=$colors->color?>"><span class="text-colors"><?= h($colors->color) ?></span></td>
				                <td><?= h($colors->created) ?></td>
				                <td><?= h($colors->modified) ?></td>
				                <td class="actions">
			                    <?= $this->Html->link(__('<i class="fa fa-info-circle"></i>'),['controller' => 'Colors', 'action' => 'view', $colors->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'tooltip','data-placement'=>'left','data-original-title'=>'Detalles']) ?>
			                    <?= $this->Html->link(__('<i class="fa fa-pencil"></i>'),['controller' => 'Colors', 'action' => 'edit', $colors->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-primary','data-toggle'=>'tooltip','data-placement'=>'right','data-original-title'=>'Editar']) ?>
			                	</td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				    </div>
				    <?php endif; ?>
				    </div>
						<?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Relacionar mas colores'), ['action' => 'edit', $product->id],['class'=>'btn btn-default', 'escape'=>false]) ?> 
				  </div>
		    </div>
		</div>
    <div class="related row">
	    <div class="column col-lg-12">
		    <div class="panel panel-default">
	    		<div class="panel-heading">Características relacionadas</div>
						<div class="panel-body">
					    <?php if (!empty($product->characteristics)): ?>
					    <div class="table-responsive">
					        <table class="table">
					            <tr>
					                <th><?= __('Name') ?></th>
					                <th><?= __('Imagen') ?></th>
					                <th><?= __('Creada') ?></th>
					                <th><?= __('Modificada') ?></th>
					                <th class="actions"><?= __('Acciones') ?></th>
					            </tr>
					            <?php foreach ($product->characteristics as $characteristics): ?>
					            <tr>
					                <td><?= h($characteristics->name) ?></td>
					                <td><?= $this->Html->image('../files/characteristics/photo/' . $characteristics->photo_dir . '/' . $characteristics->photo, ['width'=>'30']); ?></td>
					                <td><?= h($characteristics->created) ?></td>
					                <td><?= h($characteristics->modified) ?></td>
					                <td class="actions">
			                    <?= $this->Html->link(__('<i class="fa fa-info-circle"></i>'),['controller' => 'Characteristics', 'action' => 'view', $characteristics->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'tooltip','data-placement'=>'left','data-original-title'=>'Detalles']) ?>
			                    <?= $this->Html->link(__('<i class="fa fa-pencil"></i>'),['controller' => 'Characteristics', 'action' => 'edit', $characteristics->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-primary','data-toggle'=>'tooltip','data-placement'=>'right','data-original-title'=>'Editar']) ?>
			                		</td>
					            </tr>
					            <?php endforeach; ?>
					        </table>
					    </div>
					    <?php endif; ?>
			    	</div>
						<?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Relacionar mas características'), ['action' => 'edit', $product->id],['class'=>'btn btn-default', 'escape'=>false]) ?>
		    </div>
	    </div>
		</div>
		<div class="related row">
		    <div class="column col-lg-12">
			  <div class="panel panel-default">
		    		<div class="panel-heading">Tamaños relacionados</div>
						<div class="panel-body">
				    <?php if (!empty($product->sizes)): ?>
				    <div class="table-responsive">
				        <table class="table">
				            <tr>
				                <th><?= __('Nombre') ?></th>
				                <th><?= __('Imagen') ?></th>
				                <th><?= __('Creado') ?></th>
				                <th><?= __('Modificado') ?></th>
				                <th class="actions"><?= __('Acciones') ?></th>
				            </tr>
				            <?php foreach ($product->sizes as $sizes): ?>
				            <tr>
				                <td><?= h($sizes->name) ?></td>
				                <td><?= $this->Html->image('../files/sizes/photo/' . $sizes->photo_dir . '/' . $sizes->photo, ['width'=>'30']); ?></td>
				                <td><?= h($sizes->created) ?></td>
				                <td><?= h($sizes->modified) ?></td>
				                <td class="actions">
			                    <?= $this->Html->link(__('<i class="fa fa-info-circle"></i>'),['controller' => 'Sizes', 'action' => 'view', $sizes->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'tooltip','data-placement'=>'left','data-original-title'=>'Detalles']) ?>
			                    <?= $this->Html->link(__('<i class="fa fa-pencil"></i>'),['controller' => 'Sizes', 'action' => 'edit', $sizes->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-primary','data-toggle'=>'tooltip','data-placement'=>'right','data-original-title'=>'Editar']) ?>
					              </td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				    </div>
				    <?php endif; ?>
				</div>
						<?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Relacionar mas tamaños'), ['action' => 'edit', $product->id],['class'=>'btn btn-default', 'escape'=>false]) ?>
		    </div>
		    </div>
		</div>
		<div class="related row">
		    <div class="column col-lg-12">
			  <div class="panel panel-default">
		    		<div class="panel-heading">Archivos Relacionados</div>
						<div class="panel-body">
				    <?php if (!empty($product->pdfs)): ?>
				    <div class="table-responsive">
				        <table class="table">
				            <tr>
				                <th><?= __('Nombre') ?></th>
				                <th><?= __('Archivo') ?></th>
				                <th><?= __('Creado') ?></th>
				                <th><?= __('Modificado') ?></th>
				                <th class="actions"><?= __('Acciones') ?></th>
				            </tr>
				            <?php foreach ($product->pdfs as $pdfs): ?>
				            <tr>
				                <td><?= h($pdfs->name) ?></td>
				                <td><?= h($pdfs->archivo) ?></td>
				                <td><?= h($pdfs->created) ?></td>
				                <td><?= h($pdfs->modified) ?></td>
				                <td class="actions">
			                    <?= $this->Html->link(__('<i class="fa fa-info-circle"></i>'),['controller' => 'Pdfs', 'action' => 'view', $pdfs->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'tooltip','data-placement'=>'left','data-original-title'=>'Detalles']) ?>
			                    <?= $this->Html->link(__('<i class="fa fa-pencil"></i>'),['controller' => 'Pdfs', 'action' => 'edit', $pdfs->id],
				                    ['escape' => false, 'class' => 'btn btn-xs btn-primary','data-toggle'=>'tooltip','data-placement'=>'right','data-original-title'=>'Editar']) ?>
					              </td>
				            </tr>
				            <?php endforeach; ?>
				        </table>
				    </div>
				    <?php endif; ?>
				</div>
						<?= $this->Html->link(__('<i class="fa fa-plus-circle"></i> Relacionar mas archivos'), ['action' => 'edit', $product->id],['class'=>'btn btn-default', 'escape'=>false]) ?>
		    </div>
		    </div>
		</div>
</div>
